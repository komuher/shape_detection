
### This is small project used to basic shapes detection project, where i use PIL to create shapes data, keras with tensorflow to train small CNN model and matplotlib to visualization everything

#### There was some open source datasets with shapes but as task said i have to assume there isnt any dataset avalible
#### So then we have minimum 2 possible roads:
#### First scrapy data from internet
#### Second create our own dataset
#### Faster way was to creating our own dataset so that what happend here everything about creating dataset is avalible in 1creating_dataset.ipynb

#### Second problem was algorithm / architecture but that was obv. cause CNN is most popular deep learning architecture with rly good results for shape/object detection so kinda obv was to use this in this task build some small CNN model, everything avalible in 2Training_CNN.ipynb

#### And at last "Provide an easy way for non-tech-savvy people to understand whether the algorithm works or not. I.e. feed a pair of images and output whether they match​ or not (no GUI required, a simple script will be enough)."
#### I dont have much time (cause i have a lot of work on upwork at the moment :D) so i just create some simple visualization of not working and working exaple avalilbe in 3show_exaples.ipynb

#### Just download and run notebooks (for first you need to create circles, rectangles and triangles directories)
#### Then just go thru every notebook and run stuff or just watch everything on bitbucket (need to add Bitbucket Notebook Viewer first)

#### Ok thats all this project was created in ~9h of work